

//otros métodos de los arrrays

let word = 'Hola mundo';

//abstrae cada letra en un array - array.from
console.log(Array.from(word));

console.log('====================');

//split method does the same function that array.from
console.log(word.split(''));


console.log('====================');

const letters = ['b', 'c', 'z', 'a'];
const numbers = [1,8,100,300,3];

console.log(letters);
//method sort can order only letters, but not numbers.
console.log(letters.sort());
console.log('====================');
console.log(numbers.sort());


console.log('====================');
console.log('=======SORT ORDENAR=============');
//ordena menor a mayor
/*
function menorMayor(a,b){
    if(a - b < 0) return -1;
    if(a - b > 0) return 1;
    if(a == b) return 0;
    
}
*/
console.log(numbers.sort((a,b)=>a-b));

//ordena mayor a menor
/*
function mayorMenor(a,b){
    if(b - a < 0) return -1;
    if(b - a > 0) return 1;
    if(b == a ) return 0;
    
}
*/
console.log(numbers.sort((a,b)=>b-a));



console.log('=======FOR EACH=============');

const numbers2 = [12, 25, 47, 84, 98];

numbers2.forEach((element)=> console.log(element));

console.log('====================');

numbers2.forEach((number, index)=>
console.log(`${number} está en la posición ${index}`
));


console.log('====================');

const words = ['HTML', 'CSS', 'JavaScript', 'PHP'];

// - some prueba si algún elemento del arreglo cumple una confición, regresa booleano  -
console.log(words.some(word => word.length>10));

// - every prueba que cada elemento cumpla la condición, regresa booleano -
console.log(words.every(word => word.length>2));


console.log('==========MAP==========');
const numbers3 = [12, 25, 47, 84, 98];

//en el map se puede aplicar una acción a cada uno de los elementos 
numbers3.map((number) => console.log(number * 2));

const otroarraynumbers = numbers3.map((number) => number * 2);
console.log('========================')

console.log(otroarraynumbers);


/* 
//==Hacker Rank problem.
console.log('========PRUEBAS===========')
const numbers7 = [-12, -25, 0, 84, 98];

let positive = 0;
let negative = 0;
let zero = 0;

numbers7.forEach((element, index) =>{
    
    // sea = sea + element;
    
    if(Math.sign(element) === -1 ) negative++;
    else if(Math.sign(element) === 0) zero++;
    else if(Math.sign(element) === 1) positive++;
    
});

console.log(`positivos: ${positive}`);
console.log(`negativos: ${negative}`);
console.log(`zeros: ${zero}`);
console.log(numbers7.length);

console.log('===Divisiones======');
console.log((positive/numbers7.length).toPrecision(6));
console.log(negative/numbers7.length);
console.log(zero/numbers7.length);
*/

const numbers11 = [12, 25, 47, 84, 98];
//filter extrae los elementos que cumplan con la condición dada
const newArrey = numbers11.filter(number => number > 80);

console.log(newArrey);


console.log('=======reduce========')
const numbers12 = [2, 5, 7, 4, 9];
 
//reduce devuelve un solo elemento de todos los del array,
//se puede sumar, restar, multiplicar...
console.log('suma');
console.log(numbers12.reduce(( a, b )=>  a + b));

console.log('resta');
console.log(numbers12.reduce(( a, b )=>  a - b));

console.log('multiplicacion');
console.log(numbers12.reduce(( a, b )=>  a * b));

console.log('==USO DE REDUCE====');

const users = [
    {
        name: 'user 1',
        online: true
    },
    {
        name: 'user 2',
        online: false
    },
    {
        name: 'user 3',
        online: true
    },
    {
        name: 'user 4',
        online: false
    },
    {
        name: 'user 5',
        online: true
    },
    {
        name: 'user 6',
        online: true
    },
];


//filtra cada uno de los elementos del arreglo..
//revisa si la propiedad online es true
// la const cont es creada en los parametros
// la const cont es iniciada en el callback

const usersOnline = users.reduce((cont, user) => {
    if(user.online) cont++;
    return cont;
},0);

console.log(`Hay ${usersOnline} usuarios conectados`);


// El método includes() determina si una matriz incluye un determinado elemento, devuelve true o false según corresponda.
