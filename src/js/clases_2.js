class Libro{
    constructor(titulo, autor, año, genero){
        this.titulo = titulo;
        this.autor = autor;
        this.año = año;
        this.genero = genero;
    }
    getAuthor(){
        return this.autor;
    }

    getGender(){
        return this.genero
    }

    getBookInfo(){
        return `${this.titulo} ${this.autor} ${this.año} ${this.genero}`
    }
}
/* 
let Books = []
let book1;
let book2;
let book3;
for (let index = 0; index <= 2; index++) {
    let title = "";
    let year = ""; 
    let author = "";
    let genre = ""
    let isGenreValidated = false;

    while (title === "") {
        title = prompt("Write the title of Book");
    }
    while (author === "") {
        author = prompt("Write the author of Book");
    }
    while (isNaN(year) || year.toString().length !== 4) {
        year = prompt("Write the year of Book");
    }
    while (!isGenreValidated) {
        genre = prompt("Write the genre of Book: options: aventuras, terror, fantasia");
        if ( genre === 'aventuras' ) isGenreValidated = true;
        if ( genre === 'terror' ) isGenreValidated = true;
        if ( genre === 'fantasia' ) isGenreValidated = true;
    }
    
    if ( index === 0 ){
        const book1 = new Libro(title,author,year,genre);
        Books.push(book1)

    }
    if ( index === 1 ){
        const book2 = new Libro(title,author,year,genre);
        Books.push(book2)

    }
    if ( index === 3 ){
        const book3 = new Libro(title,author,year,genre);
        Books.push(book3)

    }
}

function showAllBooks(){
    for (const book of Books) {
        console.log(book)
    }
}

function showAllBooksAlphabeticalOrdered(){
    for (const book of Books) {
        console.log(book)
    }
}

showAllBooks(); */

let books = []

while (books.length < 3) {
    let title = prompt("Introudce el titulo del libro")
    let author= prompt("Introudce el autor del libro")
    let year = prompt("Introudce el año del libro")
    let gender = prompt("Introudce el genero del libro")

    if ( title != ''  && author != '' && !isNaN(year) && 
    year.length == 4 && (gender == 'aventura' || gender ==
    'terror' || gender == 'fantasia')){

        books.push(new Libro(title, author, year, gender))

    }
}

const showAllBooks = () => {
    console.log(books);
}

const showAuthors = () => {
    let authors = []
    for (const book of books) {
        authors.push(book.getAuthor());
    }

    console.log(authors.sort());
}

const showGender = () => {
    const gender = prompt('Introudce el genero a buscar')

    for (const book of books) {
        if ( book.getGender() == gender ){
            console.log(book.getBookInfo());
        }
    }

}

// showAllBooks();
// showAuthors();
showGender()