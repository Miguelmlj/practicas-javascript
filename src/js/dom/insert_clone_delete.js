/*
Insertar y eliminar elementos II

    parent.insertBefore(newElement, referenceElement) - Insertar un elemento antes del elemento de referencia

    SOPORTE TOTAL
    parent.insertAdjacentElement(position, element)
    parent.insertAdjacentElement(position, HTML)
    parent.insertAdjacentElement(position, text)

    positions:
        beforebegin - Antes de que empiece (hermano anterior)
        afterbegin - después de que empiece (primer hijo)
        beforeend - antes de que acabe (último hijo)
        afterend - después de que acabe (hermano siguiente)

    parent.replaceChild(newChild, oldChild) - Reemplaza un hijo por otro

*/

const list = document.getElementById('list');
const newElement = document.createElement('li');
newElement.textContent = "I am the new element";

/* //con appendChild el elemento se coloca hasta lo último
//list.appendChild(newElement);

//insertbedore pone el elemento antes del elemento de referencia ... 
//list.insertBefore(newElement, list.children[1]);

//si utilizamos list por padre, el elemento se insertará fuera de la lista
// list.insertAdjacentElement('beforebegin', newElement);

//si queremos que se coloque dentro con la lista, se utilizará:
// list.children[0].insertAdjacentElement('beforebegin', newElement);

//con afterbegin se colocará dentro de la lista
// list.insertAdjacentElement('afterbegin', newElement);

// con beforeend, se coloca como último hijo
// list.insertAdjacentElement('beforeend', newElement);

//
// list.children[0].insertAdjacentElement('afterend', newElement);

//con insertAdjacentHTML ... se coloca la etiqueta directo
// list.children[0].insertAdjacentHTML('afterend', '<li>Elemento con HTML</li>');

//si queremos poner solo texto. utilizamos insertAdjacentText
// list.children[1].insertAdjacentText('afterend', '<li>elemento<li>')

//PARA REMPLAZAR ELEMENTOS
//remplazar un nodo por otro
// list.replaceChild(newElement, list.children[1]); */


//LOS siguientes metodos son de las nuevas versiones de javascript, son similares a los utilizados en Jquery

/*     
    DOM manipulation convenience methods - JQuery Like
    https://caniuse.com/#search=jQuery-like


    positions:
    parent.before() - Antes de que empiece (hermano anterior) 
    parent.prepend() - después de que empiece (primer hijo)
    parent.append() - antes de que acabe (último hijo)
    parent.after() - después de que acabe (hermano siguiente)
    
    child.replaceWith(newChild)

*/

//se coloca como hermano de list
// list.before(newElement);

//para colocar dentro de la lista usaremos children
// list.children[0].before(newElement);

//para colocar después de que empiece
// list.prepend(newElement);

//para colocar antes de que acabe
// list.append(newElement);

//para colocar por fuera
// list.after(newElement);

//si ponemos childre[1] se coloca al final
// list.children[1].after(newElement);

//para remplazar un nodo
// list.children[0].replaceWith(newElement);

//remplazar obteniendo el id
// document.getElementById('child-to-replace').replaceWith(newElement);

/*

    Clonar y eliminar elementos
        element.cloneNode(true|false) - Clona el nodo. Si le pasamos true clona todo el elemento con los hijos, si le pasamos false clona solo el elemento sin hijos

        element.remove() - Elimina el nodo del DOM
        element.removeChild(child) - Elimina el nodo hijo del DOM

 */

//para clonar o hacer una copia exacta

//con true se copean los hijos
// list.after(list.cloneNode(true));

//con false no se clonan los hijos
//se sugiere copiar los elementos que no tienen un ID, sino se repetiría
// list.after(list.cloneNode(false));

//para remover el nodo
// list.remove();

//para eliminar un hijo
list.removeChild(list.children[0]);
