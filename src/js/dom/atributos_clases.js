

//atributos
//element.getAttribute('attribute')
//element.getAttribute('attribute', value)

//clases
// element.classList.add('class', 'class'. ...)
// element.classList.remove('class', 'class'. ...)
// element.classList.toggle('class', [,force])
// element.classList.contains('class')
// element.classList.replace('oldClass', newClass)

//atributos directos
// id
// value

const title = document.getElementById('title');
const nombre = document.getElementById('name');


//agrega clases
// title.classList.add('main-title', 'other-titlte');

//elimina clases
// title.classList.remove('title');

//probar si existe la clase
// if(title.classList.contains('title')) console.log('title tiene la clase title')
// else console.log('title no tiene la clase title')

//remplazar clase with .replace
// title.classList.replace('title', 'main-title');

//inner html muestra etiquetas
// console.log(title.innerHTML);
//text content muestra el texto plano sin etiquetas
// console.log(title.textContent);

/* valor del atributo value
console.log(nombre.value);
console.log(nombre.value.length); */

console.log(title);
console.log(nombre);



// poner un atributo distinto
// nombre.setAttribute('type', 'date');

// console.log(nombre);

// obtener el atributo
//console.log(name.getAttribute('type'));