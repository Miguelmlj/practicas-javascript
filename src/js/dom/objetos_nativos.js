//Objetos nativos y timers

const button = window.document.getElementById('button');
//window.document


/*
Objeto window - Es el objeto global, de él descienden todos los objetos
    alert()
    prompt()
    confirm()

*/

// window.alert('hola');

//este evento se relaciona a window y se ejecuta cuando carga la página
/* addEventListener('load', (e) => {
    console.log(e)
});

addEventListener('scroll', (e) => {
    console.log(e)
}); */


/*
Objeto console - Es el objeto que contiene la consola del navegador
    https://developer.mozilla.org/es/docs/Web/API/Console
    console.log()
    console.dir()
    console.error()
    console.table()

*/

/* const person = {
    name: 'Mike',
    age: 28,
    email: 'mike@gmail.com'
} */

/* //da opción de desplegar la información
console.dir(button);
console.error('error');

console.table(person);

//visualizar en clg table cualquier arreglo
console.table([1,2,3,4]); */

/*
    Objeto location - Es el objeto que contiene la barra de direcciones
    https://developer.mozilla.org/es/docs/Web/API/Location
    location.href
    location.protocol
    location.host
    location.pathname
    location.hash
    location.reload()

*/

/* console.log(location.href);

//if we want to know the page is http or https
console.log(location.protocol);

//with host we will get the main domain that we are visiting
console.log(location.host)

//the pathname shows the subdirectorios 
console.log(location.pathname);

//location.hash shows parameters
console.log(location.hash);

//para recargar la página
// location.reload();

//una forma de redirigir las páginas con href es
// location.href = 'https://google.com' */


/*
    Objeto history
    https://developer.mozilla.org/es/docs/DOM/Manupulando_el_historial_del_navegador

    history.back()
    history.forward()
    history.go(n| -n)
*/


/*
    Objeto date
    https://developer.mozilla.org/es/docs/Web/Javascript/Referencia/Objetos_globales/Date

    https://www.w3chools.com/jsref/jsref_obj_date.asp
*/

/* const date = new Date();

//'dia semana calendario anglosajon
console.log(date.getDay());

//día del mes
console.log(date.getDate());

console.log(date) */

/*
    Timers
        Timeout:
        https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setTimeout

        setTimeout(()=>{}, delay-in-milisecons) - Hace que se ejecute la función despues de delay. Si lo referenciamos mediante una variable/constante podemos pararlo con clearTimeout(referencia)

        Interval:
        https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setInterval

        setInterval(()=>{}, delay-in-milisecons) - Hace que se ejecute la función cada delay milisegundos. Si lo referenciamos mediante una variable/constante podemos pararlo con clearInterval(referencia)
*/

button.addEventListener('click', () => {
    //de esta manera se llaman funciones externas.
    // setTimeout(saludar,3000);

    /* setTimeout(() => {
        console.log('bye');
    }, 3000); */
    
    /* const timeout = setTimeout(() => {
        console.log('bye');
    }, 3000); */

    // clearTimeout(timeout);
    //para frenar el temporizador.. se guarda en una variable
    //y se utiliza clearTimeout

});

const saludar = () => {
    console.log('hola');

}

//setInterval

let cont = 0;

const interval = setInterval(() => {
    console.log(cont);
    cont++
}, 3000);

//para parar interval
button.addEventListener('click', () => {
    clearInterval(interval);
});




