//Recorrer el DOM (DOM Traversing)

/*
Padre - parent (Nodo del que desciende)
    - parentNode - Devuelve el nodo padre (que puede no ser un elemento)
    - parentElement - Devuelve el nodo elemento padre

    NOTA
    Los nodos del tipo Document y DocumentFragment nunca van a tener un elemento padre, parentNode devolverá siempre null.

Hijos - child (Nodo que desciende de un padre)
    - childNodes - Devuleve todos los nodos hijos
    - children - Devuelve todos los nodos hijos
    - firstChild - Devuelve el primer nodo hijo
    - firstElementChild - Devuelve el primer nodo elemento hijo
    - lastChild - Devuelve el último nodo hijo
    - lastElementChild - Devuelve el último nodo elemento hijo
    - lastChild - Devuelve el último nodo hijo
    - lastElementChild - Devuelve el último nodo elemento hijo
    - hasChildNodes() - Devuelve true si el nodo tiene hijos y false si no tiene

Hermanos - siblings (Nodo al mismo nivel)
    - nextSiblings - Devuelve el siguiente nodo hermano
    - nextElementSiblings
    - previousSibling
    - previousElementSibling
    
Cercano
    - closest(selector) - Selecciona el nodo más cercano que cumpa con el selector, aún es experimental

*/


/* const parent = document.getElementById('parent');

//parentNode regresa el padre
console.log(parent.parentNode);

//también regresa el padre
console.log(parent.parentElement);

//document regresa null porque es la parte más alta del documento, it does not have parent
console.log(document.parentElement);

//podemos ascender varios niveles... hasta html
console.log(parent.parentElement.parentElement.parentElement); */

///////HIJOS/////

/* const parent = document.getElementById('parent');

//childnodes regresa saltos de linea, texto ... /n
//console.log(parent.childNodes);

//children regresa solo los elementos, sin texto o salto de línea
console.log(parent.children);
//para selecionar un índice en particular, poner indice como en array
console.log(parent.children[1]);

//fisrt child regresa el salto de línea antes que el elemento
console.log(parent.firstChild);

//fisrt element child regresa el primer elemento
console.log(parent.firstElementChild);


//last child regresa el salto de línea antes que el elemento
console.log(parent.lastChild);

//last element child regresa el primer elemento
console.log(parent.lastElementChild);

console.log(parent.hasChildNodes());
console.log(parent.firstChild.hasChildNodes()); */

////////HERMANOS//////////
const parent = document.getElementById('parent');

//estos metodos buscan hermanos, elementos del mismo nivel, ascendente o descendente
console.log(parent.nextSibling);

console.log(parent.nextElementSibling);

console.log(parent.parentElement.nextElementSibling);

//
console.log(parent.parentElement.previousSibling)
console.log(parent.parentElement.previousElementSibling)