/**
 * document.getElementBy('id') - Acceder a un elemento a través de su id
 * 
 * document | element .querySelector('selectorCSS') - Accede al primer elemento que coincida con el selector * CSS
 * 
 * document | element .querySelectorAll('selectorCSS') - Accede a todos los elementos que coincidan con el   * selector CSS, devuelve un nodeList
 * 
 * 
 */


//getElementById
const title = document.getElementById('title');

title.textContent = 'DOM - Accediendo a nodos';


//queryselector - selecciona la clase
const paragraph = document.querySelector('.paragraph');

//const span = paragraph.querySelector("span");

//seleccionar elementos desde padre a hijos
const span = document.getElementById('section').querySelector("span");

// console.log(span.textContent);


//querySelectorAll
const paragraphs = document.querySelectorAll('.paragraph');

//con spread operator en ocasiones no es soportado por algunos browsers

//const paragraphsSpread = [...document.querySelectorAll('.paragraph')]

//forma correcta de pasar o castear el nodeList a un array
const paragraphsArray = Array.from(document.querySelectorAll('.paragraph'));

console.log(paragraphs);

let array = [...paragraphs];

console.log(array);

array.map(p => p.style.color = 'green');

paragraphsArray.map(p => p.style.color = 'red');