// DOM - Crear e insertar elements

/*
    Crear un elemento: document.createElement(element)
    Escribir texto en un elemento: element.textContent = texto
    Escribir HTML en un elemento: element.innerHTML = código HTML

    Añadir un elmento al DIM: parent.appendChild(element)

    Fragmentos de código: document.createDocumentFragment()

*/


const days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday', 'Extra Value'];

const title = document.getElementById('title');
const daysList = document.getElementById('daysList');
const selectDays = document.getElementById('daysSelect');


//crear un element de tipo HTML (EXAMPLE; elemento tipo lista)
// const itemList = document.createElement('LI');

//asignamos un valor al element HTML creado...
// itemList.textContent = 'Lunes';

//agregamos el elemento HTML con su valor agregado a dayList
/* daysList.appendChild(itemList);

console.log(itemList); */

//uso correcto de textContent e innerHTML

//textContent no puede introducir etiquetas html dentro de un documento
// title.textContent = 'DOM - <span>Crear e insertar elementos I<span>';

//innerHTML si logra insertar etiquetas
// title.innerHTML = 'DOM - <span>Crear e insertar elementos I<span>';


//ESTA manera de agregar los element HTML es incorrecta porque gasta bastantes recursos
/* for (const day of days) {
    daysList.innerHTML += `<li>${day}</li>`;
    // console.log(day);
}
 */

//esta manera es mejor ya que no gasta tantos recursos

/* const fragment = document.createDocumentFragment();

for (const day of days) {
    const itemList = document.createElement('LI');
    itemList.textContent = day;
    fragment.appendChild(itemList);
}

daysList.appendChild(fragment); */


//lo siguente es como agregar valores a un select (options)

const fragment = document.createDocumentFragment();

for (const day of days) {
    const selectItem = document.createElement('OPTION');
    selectItem.setAttribute('value', day.toLowerCase());
    selectItem.textContent = day;
    fragment.appendChild(selectItem);
    
}

selectDays.appendChild(fragment);
