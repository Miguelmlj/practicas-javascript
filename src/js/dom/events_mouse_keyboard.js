/* 
    Mouse Events:
    click - when we press the left button of mouse
    dbclick - when we press twice followed the left button of mouse
    mouseenter - when we enter on the zone that has the event
    mouseleave - when we get out of the zone that has the event
    mousedown - when we press and hold the left button of mouse
    mouseup - when we leave to press the left button of mouse

    Keyboard events:
    keydown - when we press a key
    keyup - when we leave to press a key
    keypress - when we press and hold a key



*/

const boton = document.getElementById('button');
const box = document.getElementById('box');
const input = document.getElementById('input');

/* boton.addEventListener('click', () => {
    console.log('clic')

}); */


/* boton.addEventListener('', ()=>{

}); */

/* boton.addEventListener('dblclick', ()=>{
    console.log('DOBLE CLICK');
    alert('a');

}); */

/* box.addEventListener('mouseenter', ()=>{
    // box.style.background = "green";
    box.classList.replace('red', 'green');
    
});

box.addEventListener('mouseleave', ()=>{
    // box.style.background = "red";
    box.classList.replace('green', 'red');
    
}); */

/* box.addEventListener('mousedown', () => {
    console.log('mousedown');

});

box.addEventListener('mouseup', () => {
    console.log('mouseUp');

}); */


/* box.addEventListener('mousemove', () => {
    console.log('mousemove');
});
 */

/* input.addEventListener('keydown', () => {
    console.log('keydown');
});

input.addEventListener('keyup', () => {
    console.log('keyup');
});

input.addEventListener('keypress', () => {
    console.log('keypress');
}); */