


/********************************************************** */
/*********************STAIR CASE************************************* */
/** 
const staircase = (num) => {
    let cont = num;
    for(let i = 1; i<num+1; i++){
        cont--;
        
        console.log(Array(cont).fill(' ').join('')+Array(i).fill('#').join(''));
    }
    
}

staircase(5);
*/
/********************************************************** */
/********************************************************** */


/********************************************************** */
/*********************MINI MAX SUM************************************* */
// 1. Extraer un elemento del array, sucesivamente hasta recorrer todo el arreglo - with ForEach
// 2. Sumar los elementos restantes del array - con función
// 3. Crear un otro array vacío, hacer push con el valor de la suma obtenida
// 4. imprimir el valor minimo y máximo del array resultante con Math.min max...

// const arr = [1, 2, 3, 4, 5];
// const arrResults = [];


// for (let index = 0; index < arr.length; index++) {
//     const handleArr = [...arr];

//     handleArr.splice(index,1);

//     const newArray = [...handleArr];

//      arrResults.push(newArray.reduce((a, b) => a + b));



// }


// const bigIntMax = arrResults.reduce((m, e) => e > m ? e : m);
// const bigIntMin = arrResults.reduce((m, e) => e < m ? e : m);



//   console.log(`${bigIntMin} ${bigIntMax}`);


/********************************************************** */
/********************************************************** */




/*******************BIRTHDAY CAKE CANDLES*************************************** */
/********************************************************** */

/* const candles = [0,0,0,0,0,0];

const candleHigh = candles.reduce((m, e) => e > m ? e : m);

const candlesTallest = candles.reduce((cont, user) => {
    if( user === candleHigh) cont++
    return cont;
},0);

console.log(candlesTallest); */

/*******************BIRTHDAY CAKE CANDLES*************************************** */
/********************************************************** */


//************************************************************* */
//***************** RECURSIVIDAD********************************* */

   /*  const factorial = n => {
        if(n <= 1) return 1;
        return n * factorial(n - 1);
    }

    console.log(factorial(3)); */
    


//**********************TIME CONVERSION*************************************** */
//************************************************************* */

/* const s = '01:01:00AM';
// const handleString = s;
let hora = s.substring(0,2) * 1;
let military = s.substring(2,8);

if(s.indexOf('AM') != -1){
    if(hora == 12) hora = '00' + military;
    else if( hora >= 10) hora = hora + military; 
    else hora = '0' + hora + military;
}
else{
    if(hora == 12) hora = hora + military;
    else hora = hora + 12 + military;

}


console.log(hora.toString()); */


//************************************************************* */
//******************GRADING STUDENTS*************************** */


/* 


const grades = [4, 73, 67, 38, 33];

let studentsNumber = grades.shift();
let residuo;
let diferencia;
let multiplo;
let nextMultiplo;

grades.map((grade, index) => {
    residuo = grade % 5;
    diferencia = 5 - residuo;
    nextMultiplo = grade + diferencia;

    if(((nextMultiplo - grade) >= 3) || (grade < 38)) grades[index] = grade;
    else grades[index] = grade + diferencia;

});

// return grades;

grades.map(grade => {
    console.log(grade);
    

}); */



//************************************************************* */
//************************************************************* */