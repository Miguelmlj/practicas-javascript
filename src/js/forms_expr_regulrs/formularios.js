const form = document.getElementById('form');
const button = document.getElementById('submitButton');

const nombre = document.getElementById('name');
const email = document.getElementById('email');
const gender = document.getElementById('gender');
const terms = document.getElementById('terms');

const formIsValid = {
    nombre: false,
    email: false,
    gender: false,
    terms: false,
};

form.addEventListener('submit', (e) => {
    e.preventDefault();
    validateForm();
})

nombre.addEventListener('change', (e) => {
    if(e.target.value.trim().length > 0) formIsValid.nombre = true
})

email.addEventListener('change', (e) => {
    if(e.target.value.trim().length > 0) formIsValid.email = true;
})

gender.addEventListener('change', (e) => {
    if(e.target.checked == true) formIsValid.gender = true;
})

terms.addEventListener('change', (e) => {
    formIsValid.terms = e.target.checked;
    e.target.checked ? button.removeAttribute('disabled') : 
    button.setAttribute('disabled', true);
})

const validateForm = () => {
    //Object.keys
    const formValues = Object.values(formIsValid);
    const valid = formValues.findIndex(value => value == false);
    if(valid == -1) form.submit()
    else alert('form invalid')
}