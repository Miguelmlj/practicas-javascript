const person = {
    name: 'Juan',
    age: 26,
    sons: ['Laura','Diego','Pepe','Rosa','Tomás']
}

console.log(person.name);
console.log(person['name']);


console.log('===================');

for (const key in person) {
    console.log(key);
}

console.log('===================');
for (const key in person){
    console.log(person[key]);
}


console.log('===================');

for (const son of person.sons){
    console.log(son);
}


console.log('===================');

console.log(`Hola ${person.name}. Tienes ${person.age} años y tus hijos se llaman ${person.sons.join(', ')}`);