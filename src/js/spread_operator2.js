//spread operator

const person1 = {
    name: "Juan",
    age: 46
}

/* //de esta manera puedes actualizar datos...
const person2 = { ...person1, age: 47, name: "updated"} */


/* //seguimiento tuto
const person2 = { ...person1, id: 123}

console.log(person1);
console.log(person2); */

const cities1 = [
    "Medellin", "Bogota", "Cali"
]

const cities2 = [
    "Barranquilla", "Cartagena", "SantaMarta"
]

console.log(cities1);
console.log(cities2);

const join = [...cities1, ...cities2];

console.log(join);