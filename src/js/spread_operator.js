//spread operator

const numbers = [-12, 2, 3, 23, 43, 2, 3];

console.log(numbers);
console.log('===============');
console.log(...numbers);


const addNumbers = (a,b,c) => {
    console.log(a+b+c);
}

let numbersToAdd = [1,2,3];

addNumbers(...numbersToAdd);


console.log('===============');

let users = ['javier','david','rosa','juan','mercedes'];

let newUsers = ['marta', 'jaime', 'laura'];

//users.push(newUsers[0], newUsers[1], newUsers[2]);

//introducir elementos de un arrray a otro con
//spread operator

users.push(...newUsers);

console.log(users);

console.log('=======copiar array========');

let arr = [1,2,3,4,5];

let arr2 = [...arr];

console.log(arr2);

console.log('=======concatenar array========');

let arr3 = [1,2,3,4,5];

let arr4 = [6,7,8];

//traditional way
let arrConcat = arr3.concat(arr4);

console.log(arrConcat);

//another way to concatenate arrays
console.log('=======concatenar array 2========');

let arr5 = [1,2,3,4,5];

let arr6 = [6,7,8];

let arrConcat2 = [...arr5, ...arr6];

console.log(arrConcat2);


console.log('=======parametros REST========');
//enviar un número indefinido de argumentos a una función

//analize this...
const restParams = (...numbersone) => {
    console.log(numbersone);
    console.log(...numbersone);
}

restParams(1,2,3,4,5);

console.log('=======using libería math========');

const otronumbers = [-12, 2, 3, 23, 43, 2, 3];

//get the highest value
console.log(Math.max(...otronumbers));
//get the lowest value
console.log(Math.min(...otronumbers));

console.log('=======Eliminar elementos duplicados========');

// obtiene el array como tipo set, sin repetir elementos
console.log(new Set(otronumbers));
console.log('==set a array===')
//obtener el array sin números duplicados, primero se pasa a set y después se convierte a un array poniendo entre corchetes y utilizando el spread operator

console.log([...new Set(otronumbers)]);