let arrayNum = [1,2,3,4,5];

//console.log(arrayNum);


//métodos arrrays
//Array.isArray(valor a evalurar)
let num = 2;

console.log(Array.isArray(num));
console.log(Array.isArray(arrayNum));
console.log('=====================')
console.log('=====================')

//eliminar primer elemento del array con  .shift   y el último con .pop   y el elemento se puede almacenar
//
let otroarray = [1,2,3,4,5];
console.log(otroarray);
let e =otroarray.shift();
console.log(e);
console.log(otroarray);
let a = otroarray.pop();
console.log(a);
console.log(otroarray);

console.log('=====================')
console.log('=====================')
//metodos push y unshif añaden elementos y devuelve la longitud del arrray
let otroarreglo = [1,2,3,4,5];
console.log(otroarreglo);
otroarreglo.push(7);
console.log(otroarreglo);
let longitud = otroarreglo.unshift(2);
console.log(otroarreglo);
console.log(longitud);

console.log('=====================')
console.log('=====================')

//metodos .indexOf() e .lastIndexOf() encuentran elementos y regresa el índice donde se encuentra tal elemento
let otroarreglo1 = [1,2,3,4,5,2,3];
console.log(otroarreglo1);
console.log(otroarreglo1.indexOf(3)); //si no encuentra indice regresa -1

console.log(otroarreglo1.lastIndexOf(3))

console.log('=====================')
console.log('=====================')

//.reverse()  invierte el orden de los elemnetos del arreglo
let otroarreglo2 = [1,2,3,4,5,2,3];
console.log(otroarreglo2);
otroarreglo2.reverse();
console.log(otroarreglo2);

console.log('=====================')
console.log('=====================')

// método .join('separador') - Devuelve un string con el separador que indiquemos, por defecto son comas
let otroarreglo3 = [1,2,3,4,5,2,3];
console.log('método join');
console.log(otroarreglo3);
let arrayString = otroarreglo3.join(' ');
console.log(arrayString);
console.log(otroarreglo3.join('-'));

console.log('=====================')
console.log('=====================')
//metodo splice
let otroarreglo4 = [1,2,3,4,5];
console.log(otroarreglo4);
otroarreglo4.splice(3); //elimina desde la posición a hasta el final
console.log(otroarreglo4);

console.log('=====================')
let otroarreglo5 = [1,2,3,4,5];
console.log(otroarreglo5);
otroarreglo5.splice(2,2); //elimina desde la posición a el número de valores que le indiquemos 
console.log(otroarreglo5);


console.log('=====================')
let otroarreglo6 = [1,2,3,4,5];
console.log(otroarreglo6);
otroarreglo6.splice(2,2,10,23,54); //si b es un valor distinto de 0 elimina el número de valores que indiquemos en b y añade los valores de items a partir de la posicion a
console.log(otroarreglo6);


console.log('=====================')
let otroarreglo7 = [1,2,3,4,5];
console.log(otroarreglo7);
otroarreglo7.splice(3,0,10,23,54); //si b vale 0 añade los elementos a partir de la posicion a y no elimina ninguno
console.log(otroarreglo7);


//método slice = extrae otro array del mimso

console.log('=========slice============')
let otroarreglo8 = [1,2,3,4,5];
console.log(otroarreglo8);

let newarreglo = otroarreglo8.slice();
console.log(newarreglo);


console.log('=========slice============')
let otroarreglo9 = [1,2,3,4,5];
console.log(otroarreglo9);

let newnewarreglo = otroarreglo9.slice(2);
console.log(newnewarreglo);


console.log('=========slice============')
let otroarreglo10 = [1,2,3,4,5,6,7];
console.log(otroarreglo10);

let newnuevoarreglo = otroarreglo10.slice(2,4);
console.log(newnuevoarreglo);


//MÉTODO FIND
//El método find() devuelve el valor del primer elemento del array que cumple la función de prueba proporcionada.

/* const array1 = [5, 12, 8, 130, 44];

const found = array1.find(element => element > 10);

console.log(found);
// expected output: 12
 */



