//un callback es una función que se ejecuta a través de otra función
//no son asincronos

/* 
    PRIMER EJEMPLO

const getUser = (id, cb) => {
    const user = {
        name: 'miguel',
        id:id
    }

    //si error
    // cb(null, user);

    //con error
    // cb('User not exist')

    //con validacion
    if(id == 2)  cb('User nos exist');
    else cb(null, user);
}


getUser(1, (err, user) => {
    if(err) return console.log(err)
    console.log(`User name is ${user.name}`);

}); */


const users = [
    {
        id: 1,
        name: 'Dorian'
    },

    {
        id: 2,
        name: 'Laura'
    },
    {
        id: 3,
        name: 'Carlos'
    },

]

const emails = [
    {
        id: 1,
        email: 'dorian@gmail.com'
    },
    {
        id: 2,
        email: 'Laura@gmail.com'
    }
]


const getUser = (id, cb) => {
    const user = users.find(user => user.id == id);
    if (!user) cb(`No exist a user with id ${id}`);
    else cb(null, user);

}

const getEmail = (user, cb) => {
    const email = emails.find(email => email.id == user.id);
    if(!email) cb(`${user.name} has not email`);
    else cb(null, {
        id: user.id,
        name: user.name,
        email: email.email
    })
}

getUser(1, (err, user) => {
    if (err) return console.log(err);
    
    getEmail(user, (err, res) => {
        if(err) return console.log(err);
        console.log(res);
    })
})

