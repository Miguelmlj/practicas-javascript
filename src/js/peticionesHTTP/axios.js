const button = document.getElementById('button');
const button2 = document.getElementById('button2');

button.addEventListener('click', () => {
    axios({
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/users'
    }).then(res => {
        // console.log(res.data);
        const list = document.getElementById('list');
        const fragment = document.createDocumentFragment()
        
        for(const userInfo of res.data) {
            const listItem = document.createElement('LI')
            listItem.textContent = `${userInfo.id} - ${userInfo.name}`
            fragment.appendChild(listItem)
        }

        list.appendChild(fragment)

    }).catch(err => console.log(err))
})

button2.addEventListener('click', () => {
    axios({
        method: 'POST',
        url: 'https://jsonplaceholder.typicode.com/posts',
        data: {
            title: 'A new post',
            body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            userId: 1
        }
    }).then(res => console.log(res))
        .catch(err => console.log(err))
})