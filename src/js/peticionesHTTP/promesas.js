//una promesa es un objeto con 2 callbacks internos

/* 
    PRIMER EJEMPLO

const getUser = (id, cb) => {
    const user = {
        name: 'miguel',
        id:id
    }

    //si error
    // cb(null, user);

    //con error
    // cb('User not exist')

    //con validacion
    if(id == 2)  cb('User nos exist');
    else cb(null, user);
}


getUser(1, (err, user) => {
    if(err) return console.log(err)
    console.log(`User name is ${user.name}`);

}); */


const users = [
    {
        id: 1,
        name: 'Dorian'
    },

    {
        id: 2,
        name: 'Laura'
    },
    {
        id: 3,
        name: 'Carlos'
    },

]

const emails = [
    {
        id: 1,
        email: 'dorian@gmail.com'
    },
    {
        id: 2,
        email: 'Laura@gmail.com'
    }
]


const getUser = (id) => {
    const user = users.find(user => user.id == id);

    return promise = new Promise((resolve, reject) => {
        if (!user) reject(`Does not exist an user with id ${id}`);
        else resolve(user);

    })

}

const getEmail = (user) => {
    const email = emails.find(email => email.id == user.id);

    return promise = new Promise((resolve, reject) => {
        if (!email) reject(`${user.name} has no email`);
        else resolve({
            id: user.id,
            name: user.name,
            email: email.email
        });

    })

}



//PROMESA FORMA EXTENSA
/* getUser(2)
    .then(user => getEmail(user))
    .then(res => console.log(res))
    .catch(err => console.log(err));
 */


//PROMESA FORMA CORTA, COMO SOLO ESPERA UN RESULTADO, NO HACE FALTA PASARLO EN LA FUNCION, COMO PARAMETRO
    getUser(2)
    .then(getEmail)
    .then(console.log)
    .catch(console.log);




