//operador ternario

let num = 2;

(num % 2 == 0) ? console.log(`${num} es par`) : console.log(`${num} es impar`); 


console.log('====================================')
console.log('====================================')

// (num % 2 === 0) ? 
//     (
//     console.log(`${num} es par`),
//     console.log(`${num} es par 2`) 
//     )
//     : 
//     (
//     console.log(`${num} es impar`), 
//     console.log(`${num} es impar 2`)
//     );


//OTRA MANERA DE OPERADOR TERNARIO
//ES PARA DAR SOLO LA RESPUESTA POSITIVA... SIN EL ELSE

const y = 22;

y > 2 ? console.log("y es mayor a 2") : console.log("Y es menor o giaul 2 ");

y > 2 && console.log("Y es mayor a 2");